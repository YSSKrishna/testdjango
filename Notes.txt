virtualenv venv

pip install djangorestframework

pip freeze > requirements.txt

django-admin startproject testDjango .

python manage.py makemigrations
python manage.py migrate

python manage.py runserver

python manage.py startapp api_basic

python manage.py createsuperuser
username : admin
password : pass@1234

python manage.py shell

from api_basic.models import Article
from api_basic.serializers import ArticleSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

a = Article(title="Kill me Again", author="SSK", email="dummy@bummy.com")
a.save()


serializer = ArticleSerializer(a)
serializer.data

content =  JSONRenderer().render(serializer.data)
content

serializer = ArticleSerializer(Article.objects.all(), many=True)


******Model Serializer *************
Serializer = ArticleSerializer()
print(repr(serializer))

******Function based API Views ****
