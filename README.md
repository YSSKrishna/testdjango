# Lets roll

## Install dependencies
pip install requirements.txt

## Start server 
python manage.py runserver

## ArticleAPIView
http://127.0.0.1:8000/article/

## ArticleDetailAPIView
http://127.0.0.1:8000/article/1/

## GenericAPIView
http://127.0.0.1:8000/generic/article/

## Viewset
http://127.0.0.1:8000/viewset/article/